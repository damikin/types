structure Bool =
struct
(*
t ::=
	true
	false
	if t then t else t

v ::=
	true
	false

evaluation: t -> t'

E-IfTrue
	if true then t2 else t3 -> t2

E-IfFalse
	if false then t2 else t3 -> t3

E-If
	t -> t'
	------
	if t then t2 else t3 ->
	if t' then t2 else t3
*)
exception Done

datatype Terms =
	True
	| False
	| If of Terms * Terms * Terms

fun onestep (If(True, t, _)) = t
	| onestep (If(False, _, t)) = t
	| onestep (If(t1, t2, t3)) = If(onestep t1, t2, t3)
	| onestep _ = raise Done

fun reduce t =
	let val t' = onestep t
	in reduce t'
	end
	handle Done => t

fun print_t t =
	let
		fun tab s 0 = s
			| tab s c = tab (s ^ "  ") (c - 1)

		fun pr s c = print ((tab "" c) ^ s ^ "\n")

		fun p True c = pr "true" c
			| p False c = pr "false" c
			| p (If(t1, t2, t3)) c =
			(
				pr "If" c;
				p t1 (c + 1);
				pr "Then" c;
				p t2 (c + 1);
				pr "Else" c;
				p t3 (c + 1)
			)
	in
		p t 0
	end

(* testing *)
val t1 = True
val t2 = If(True, False, True)
val t3 = If(False, t1, t2)
val t4 = If(If(t2, t1, t3), False, True)

(* True *)
fun test1 () = reduce t1
(* False *)
fun test2 () = reduce t2
(* False *)
fun test3 () = reduce t3
(* True *)
fun test4 () = reduce t4

end

