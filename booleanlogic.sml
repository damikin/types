structure Bool =
struct
(*
t ::=
	true
	false
	t or t
	t and t
	not t

## evaluation: t -> t'

E-OrTrue
	true or t -> true

E-OrFalse
	false or t -> t

E-Or
	t -> t'
	------
	t1 or t2 -> t1' or t2

E-AndFalse
	false and t -> false

E-AndTrue
	true and t -> t

E-And
	t -> t'
	------
	t1 and t2 -> t1' and t2

E-NotTrue
	not true -> false

E-NotFalse
	not false -> true

E-Not
	t -> t'
	------
	not t -> not t'
*)
exception Done

datatype Terms =
	True
	| False
	| Or of Terms * Terms
	| And of Terms * Terms
	| Not of Terms

fun onestep (Or(True, _)) = True
	| onestep (Or(False, t)) = t
	| onestep (Or(t1, t2)) = Or(onestep t1, t2)
	| onestep (And(False, _)) = False
	| onestep (And(True, t)) = t
	| onestep (And(t1, t2)) = And(onestep t1, t2)
	| onestep (Not True) = False
	| onestep (Not False) = True
	| onestep (Not t) = Not (onestep t)
	| onestep _ = raise Done

fun reduce t =
	let val t' = onestep t
	in reduce t'
	end
	handle Done => t

fun print_t t =
	let
		fun p True = "True"
			| p False = "False"
			| p (Or(t1, t2)) = "(" ^ (p t1) ^ " Or " ^ (p t2) ^ ")"
			| p (And(t1, t2)) = "(" ^ (p t1) ^ " And " ^ (p t2) ^ ")"
			| p (Not t) = "(Not " ^ (p t) ^ ")"
	in
		print ((p t) ^ "\n")
	end

val t1 = True
val t2 = Not False
val t3 = Not (Not True)

(* True *)
fun test1 () = reduce t1
(* True *)
fun test2 () = reduce t2
(* True *)
fun test3 () = reduce t3
(* true (equal) *)
fun testeq () = (test2 ()) = (test3 ())

(* De Morgan's Law *)
fun demorgans () =
	let
		(* !(a && b) *)
		fun f1 a b = Not (And(a, b))
		(* !a || !b *)
		fun f1' a b = Or(Not a, Not b)
		(* !(a || b) *)
		fun f2 a b = Not (Or(a, b))
		(* !a && !b *)
		fun f2' a b = And(Not a, Not b)
		(*
			we are brute forcing,
			so each combination gets tested
		*)
		fun helper a b =
			let
				val t1 = f1 a b
				val t1' = f1' a b
				val t2 = f2 a b
				val t2' = f2' a b
			in
				(reduce t1) = (reduce t1') andalso (reduce t2) = (reduce t2')
			end
		
		val truetrue = helper True True
		val truefalse = helper True False
		val falsetrue = helper False True
		val falsefalse = helper False False
	in
		(* all of them need to be true *)
		truetrue andalso truefalse andalso falsetrue andalso falsefalse
	end

end

