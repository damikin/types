structure Lambda =
struct
(*
t ::=
	x
	\x.t
	t t

v ::=
	x
	\x.t

## evaluations: t -> t'

E-App1
	t1 -> t1'
	--------
	t1 t2 -> t1' t2

E-App2
	t2 -> t2'
	--------
	v1 t2 -> v1 t2'

E-AppAbs
	(\x.t12) v2 -> [x->v2]t12
	# simply, replace all x with v2 in t12

E-AppAbs2
	# full beta and normal order
	t12 -> t12'
	----------
	\x.t12 -> \x.t12'
*)
exception Done

datatype Terms =
	Var of int
	| Abs of Terms * Terms
	| App of Terms * Terms

(* helpers *)
fun inc n (Var i) = Var (i + n)
	| inc n (Abs(t1, t2)) = Abs(inc n t1, inc n t2)
	| inc n (App(t1, t2)) = App(inc n t1, inc n t2)

fun app t1 t2 =
	let
		fun max (f:int -> int) (Var i) (m:int) = f (Int.max(m, i))
			| max f (Abs(t1, t2)) m = max (max f t2) t1 m
			| max f (App(t1, t2)) m = max (max f t2) t1 m
		fun i t = (max (fn x => x) t 0) +1
	in
		App(t1, inc (i t1) t2)
	end

fun rewrite (Abs(Var i1, t1)) t2 =
	let
		fun r (Var i) = if i1 = i then t2 else Var i
			| r (Abs(v, t)) = Abs(v, r t)
			| r (App(a, t)) = App(r a, r t)
	in
		r t1
	end

fun onestep (App(t1 as (Abs _), t2)) = rewrite t1 t2
	| onestep (App(t1 as (Var _), t2)) = App(t1, onestep t2)
	| onestep (App(t1 as (App _), t2)) = App(onestep t1, t2)
	| onestep (Abs(Var i1, t2)) = Abs(Var i1, onestep t2)
	| onestep _ = raise Done

(* reducer *)
fun reduce t =
	let val t' = onestep t
	in reduce t'
	end
	handle Done => t

(* combinator calculus *)
(*
	I: \x.x
	K: \xy.x
	S: \xyz.(x z) (y z)
*)
val I = Abs(Var 0, Var 0)
val K = Abs(Var 0, Abs(Var 1, Var 0))
val S = Abs(Var 2, Abs(Var 1, Abs(Var 0,
	App(
		App(Var 2, Var 0),
		App(Var 1, Var 0)))))
val SKK = app (app S K) K

(* church numerals *)
(*
	succ: \xyz.y (x y z)
	add: \wxyz.(w y) (x y z)
	mul: \xyz.x (y z)
*)
val succ = Abs(Var 2, Abs(Var 1, Abs(Var 0,
	App(Var 1,
		App(App(Var 2, Var 1), Var 0)))))

val add = Abs(Var 3, Abs(Var 2, Abs(Var 1, Abs(Var 0,
	App(
		App(Var 3, Var 1),
		App(
			App(Var 2, Var 1),
			Var 0))))))

val mul = Abs(Var 2, Abs(Var 1, Abs(Var 0,
	App(
		Var 2,
		App(Var 1, Var 0)))))

fun church n =
	let
		fun c 0 t = t
			| c n t = c (n-1) (app succ t)
	in
		c n (Abs(Var 1, Abs(Var 0, Var 0)))
	end

fun unchurch (Abs(Var i, t2)) =
		let
			fun cnt (Var i') = if i = i' then 1 else 0
				| cnt (App(t1, t2)) = (cnt t1) + (cnt t2)
				| cnt (Abs(t1, t2)) = (cnt t1) + (cnt t2)
		in
			cnt t2
		end
	| unchurch _ = (print "expecting \\x.t, didn't get"; 0)

val n0 = church 0
val n1 = church 1
val n2 = church 2
val n3 = church 3
val n4 = church 4
val n5 = church 5

(* pretty printing *)
fun print_t t =
	let
		fun p (Var i) = Int.toString i
			| p (Abs(t1, t2)) = "\\" ^ (p t1) ^ "." ^ (p t2)
			| p (App(t1, t2)) = "(" ^ (p t1) ^ " " ^ (p t2) ^ ")"
	in
		(print ((p t) ^ "\n"); t)
	end

fun print_cn t =
	let
		val s = Int.toString (unchurch t)
	in
		print (s ^ "\n")
	end

(* tests *)
val t1 =
	let
		fun idapp t = app I t
	in
		idapp (idapp (Abs(Var 0, App(inc 1 I, Var 0))))
	end

val t2 = SKK
val t3 = List.map (fn x => reduce x) [n0, n1, n2, n3, n4, n5]
val t4 = app (app add n0) n0
val t5 = app (app add n2) n3
val t6 = app (app mul n2) n3

(* \2.2 *)
fun test1 () = print_t (reduce t1)
(* \0.0 *)
fun test2 () = print_t (reduce t2)
(*
	\1.\0.0
	\1.\0.(1 0)
	\1.\0.(1 (1 0))
	...etc...
*)
fun test3 () = List.map (fn x => (print_t x; print_cn x; x)) t3
(* \1.\0.0 *)
fun test4 () =
	let val a = reduce t4
	in (print_t a; print_cn a)
	end
(* \1.\0.(1 (1 (1 (1 (1 0))))) *)
fun test5 () =
	let val a = reduce t5
	in (print_t a; print_cn a)
	end
(* \0.\3.(0 (0 (0 (0 (0 (0 3)))))) *)
fun test6 () =
	let val a = reduce t6
	in (print_t a; print_cn a)
	end

end

