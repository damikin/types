structure Numbool =
struct
(*
t ::=
	true
	false
	if t then t else t
	0
	succ t
	pred t
	iszero t

v ::=
	true
	false
	nv

nv ::=
	0
	succ nv

## evaluation: t -> t'

E-IfTrue
	if true then t2 else t3 -> t2

E-IfFalse
	if false then t2 else t3 -> t3

E-If
	t -> t'
	------
	if t then t2 else t3 ->
	if t' then t2 else t3

E-Succ
	t -> t'
	------
	succ t -> succ t'

E-PredZero
	pred 0 -> 0

E-PredSucc
	pred (succ nv) -> nv

E-Pred
	t -> t'
	------
	pred t -> pred t'

E-IszeroZero
	iszero 0 -> true

E-IszeroSucc
	iszero (succ nv) -> false

E-IsZero
	t -> t'
	------
	iszero t -> iszero t'
*)
exception Done

datatype Terms =
	True
	| False
	| If of Terms * Terms * Terms
	| Zero
	| Succ of Terms
	| Pred of Terms
	| Iszero of Terms

fun isnum Zero = true
	| isnum (Succ t) = isnum t
	| isnum _ = false

fun onestep (If(True, t, _)) = t
	| onestep (If(False, _, t)) = t
	| onestep (If(t1, t2, t3)) =
		if isnum t1
		then raise Done
		else If(onestep t1, t2, t3)
	| onestep (Succ t) =
		if isnum t
		then raise Done
		else Succ (onestep t)
	| onestep (Pred Zero) = Zero
	| onestep (Pred t) =
		(case t of
			Succ t' =>
				if isnum t'
				then t'
				else Pred (onestep t)
			| _ => Pred (onestep t))
	| onestep (Iszero Zero) = True
	| onestep (Iszero t) =
		(case t of
			Succ t' =>
				if isnum t'
				then False
				else Iszero (onestep t)
			| _ => Iszero (onestep t))
	| onestep _ = raise Done

fun reduce t =
	let val t' = onestep t
	in reduce t'
	end
	handle Done => t

fun print_t t =
	let
		fun tab s 0 = s
			| tab s c = tab (s ^ "  ") (c - 1)

		fun pr s c = print ((tab "" c) ^ s ^ "\n")

		fun p True c = pr "true" c
			| p False c = pr "false" c
			| p (If(t1, t2, t3)) c = (
				pr "If" c;
				p t1 (c + 1);
				pr "Then" c;
				p t2 (c + 1);
				pr "Else" c;
				p t3 (c + 1))
			| p Zero c = pr "0" c
			| p (Succ t) c = (
				pr "succ" c;
				p t (c + 1))
			| p (Pred t) c = (
				pr "pred" c;
				p t (c + 1))
			| p (Iszero t) c = (
				pr "iszero" c;
				p t (c + 1))
	in
		p t 0
	end

(* testing *)
val t1 = True
val t2 = If(True, False, True)
val t3 = If(False, t1, t2)
val t4 = If(If(t2, t1, t3), False, True)
val t5 = Pred(Succ(Pred Zero))
val t6 = If(t5, True, False)
val t7 = If(Iszero Zero, Succ Zero, Pred Zero)
val t8 = If(Iszero t6, t5, t3)

(* True *)
fun test1 () = reduce t1
(* False *)
fun test2 () = reduce t2
(* False *)
fun test3 () = reduce t3
(* True *)
fun test4 () = reduce t4
(* Zero *)
fun test5 () = reduce t5
(* If (Zero,True,False) *)
fun test6 () = reduce t6
(* Succ 0 *)
fun test7 () = reduce t7
(* If (Iszero If (Zero,True,False), Zero, False) *)
fun test8 () = reduce t8

end

